////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/type/type_list_expander.h>

int main()
{
	bool result = true;

	bad::type_list_expander<bad::type_list<int, char, double>>{}(
		[&result] (const int i, const char c, const double d) noexcept
		{
			result &= i == 10 && c == 'c' && d == .1;
		},
		{10, 'c', .1}
	);

	return !result;
}

