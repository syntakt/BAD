////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/container/flat_set.h>
#include <iostream>

struct compare {
	bool operator ()(const int a, const int b) const noexcept
	{
		return a < b;
	}
};

void print_flat_set(const bad::flat_set<int, compare>& flat_set) noexcept
{
	for (const int i : flat_set)
		std::cout << i << ' ';
	std::cout << std::endl;
}

int main()
{
	bool result = true;

	bad::flat_set<int, compare> ints;
	ints.insert(0);
	ints.insert(2);
	ints.insert(0);
	ints.insert(8);
	ints.insert(2);
	ints.insert(4);
	ints.insert(2);
	ints.insert(100);
	print_flat_set(ints);

	result &= ints[0] == 0 && ints[1] == 2 && ints[2] == 4 && ints[3] == 8 && ints[4] == 100;

	ints.erase(5);
	ints.erase(8);
	ints.erase(0);
	ints.erase(100);
	print_flat_set(ints);

	result &= ints[0] == 2 && ints[1] == 4;

	return !result;
}
