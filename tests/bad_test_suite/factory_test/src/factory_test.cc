////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/factory.h>
#include <memory>

int main()
{
	class a {
		public:
		a() noexcept
		{
		}

		virtual ~a()
		{
		}

		virtual std::string get_name() const noexcept = 0;
	};

	class b final : public a {
		public:
		b() noexcept :
			a{}
		{
		}

		~b() override
		{
		}

		std::string get_name() const noexcept override
		{
			return "b";
		}
	};

	class c final : public a {
		public:
		c() noexcept :
			a{}
		{
		}

		~c() override
		{
		}

		std::string get_name() const noexcept override
		{
			return "c";
		}
	};

	bad::factory<a*> factory;
	factory.register_creator("b", [] { return new b; });
	factory.register_creator("c", [] { return new c; });
	const std::unique_ptr<a> b_ptr{factory.create("b")},
	                         c_ptr{factory.create("c")};

	const bool result = b_ptr->get_name() == "b" && c_ptr->get_name() == "c";
	return !result;
}
