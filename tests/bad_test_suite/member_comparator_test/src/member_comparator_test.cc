////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <algorithm>
#include <bad/container/member_comparator.h>

int main()
{
	struct operand final {
		int get() const noexcept { return i; }

		int i;
	};

	const auto sort_and_test = [] (std::vector<operand> operands, auto&& member_comparator) noexcept
	{
		std::sort(operands.begin(), operands.end(), member_comparator);

		return operands[0].i == 0 &&
		       operands[1].i == 1 &&
		       operands[2].i == 2 &&
		       operands[3].i == 3;
	};

	const std::vector<operand> operands{{1}, {3}, {2}, {0}};
	const bool result = sort_and_test(operands, bad::member_comparator<&operand::get>{}) &&
	                    sort_and_test(operands, bad::member_comparator<&operand::i>{});

	return !result;
}
