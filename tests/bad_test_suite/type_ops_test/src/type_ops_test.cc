////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/type/type_ops.h>

namespace
{

consteval bool test_remove_pointers() noexcept
{
	bool result = true;

	result &= std::same_as<bad::remove_pointers<int*>::type, int>;
	result &= std::same_as<bad::remove_pointers<const int*>::type, const int>;
	result &= std::same_as<bad::remove_pointers<int* const>::type, int>;
	result &= std::same_as<bad::remove_pointers<const int* const>::type, const int>;
	result &= std::same_as<bad::remove_pointers<int* const>::type, int>;

	result &= std::same_as<bad::remove_pointers<int**>::type, int>;
	result &= std::same_as<bad::remove_pointers<const int**>::type, const int>;
	result &= std::same_as<bad::remove_pointers<int* const * const>::type, int>;
	result &= std::same_as<bad::remove_pointers<const int* const * const>::type, const int>;

	result &= std::same_as<bad::remove_pointers<int*&>::type, int&>;
	result &= std::same_as<bad::remove_pointers<int*&&>::type, int&&>;
	result &= std::same_as<bad::remove_pointers<const int*&>::type, const int&>;
	result &= std::same_as<bad::remove_pointers<const int*&&>::type, const int&&>;

	return result;
}

consteval bool test_nude() noexcept
{
	bool result = true;

	result &= std::same_as<bad::nude<int>::type, int>;
	result &= std::same_as<bad::nude<const int>::type, int>;
	result &= std::same_as<bad::nude<int&>::type, int>;
	result &= std::same_as<bad::nude<const int&>::type, int>;
	result &= std::same_as<bad::nude<int&&>::type, int>;
	result &= std::same_as<bad::nude<int*>::type, int>;
	result &= std::same_as<bad::nude<const int*>::type, int>;
	result &= std::same_as<bad::nude<int*&>::type, int>;
	result &= std::same_as<bad::nude<int* const&>::type, int>;

	return result;
}

}// namespace

int main()
{
	bool result = true;

	result &= test_remove_pointers();
	result &= test_nude();

	return !result;
}
