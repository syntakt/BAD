////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/type/type_list.h>

int main()
{
	using test_type_list = bad::type_list<int, char, double>;

	bool result = true;
	test_type_list::for_each(
		[i = 0, &result] (const auto& element) mutable noexcept
		{
			using element_type = std::remove_cvref_t<decltype(element)>::type;

			if (i == 0)
				result &= std::is_same_v<element_type, int>;
			else if (i == 1)
				result &= std::is_same_v<element_type, char>;
			else if (i == 2)
				result &= std::is_same_v<element_type, double>;

			++i;
			return false;
		}
	);
	result &= test_type_list::contains<int, char, double>();

	return !result;
}
