////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/type/type.h>

struct my_struct final
{
};

int main()
{
	constexpr const bad::type_id int_type_id = bad::id_type<int>(),
	                             float_type_id = bad::id_type<float>(),
	                             my_struct_type_id = bad::id_type<my_struct>();

	const bool result = int_type_id != float_type_id &&
	                    int_type_id != my_struct_type_id &&
	                    float_type_id != my_struct_type_id;

	return !result;
}
