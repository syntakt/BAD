////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/string/string_ops.h>

int main()
{
	std::string str = "I was truly so lucky to meet you, thank you for being here.";
	const bool result = bad::str::replace_all(str, "you", "(u)") == 2 &&
	                    str == "I was truly so lucky to meet (u), thank (u) for being here.";
	return !result;
}
