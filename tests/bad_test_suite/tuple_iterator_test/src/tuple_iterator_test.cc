////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <tuple>
#include <bad/type/type.h>
#include <bad/container/tuple_iterator.h>

int main()
{
	struct test_type final {};

	bool result = true;

	const std::tuple<int, float, test_type> tuple{};
	bad::tuple_iterator{tuple}(
		[&result, i = 0] <typename t> ([[maybe_unused]] const t& value) mutable noexcept
		{
			constexpr const bad::type_id type_ids[]{
				bad::id_type<int>(),
				bad::id_type<float>(),
				bad::id_type<test_type>()
			};

			result &= type_ids[i++] == bad::id_type<t>();
			return false;
		}
	);

	return !result;
}
