////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/type/type.h>
#include <iostream>

class simple_class final {
	public:
	class nested_class final {
	};
};

namespace ns
{

class namespaced_class final {
};

}// namespace ns

template <typename T>
class template_class final {
};

using alias = simple_class;

int main()
{
	constexpr const std::string_view char_type_name = bad::name_type<char>(),
	                                 schar_type_name = bad::name_type<signed char>(),
	                                 uchar_type_name = bad::name_type<unsigned char>(),
	                                 short_type_name = bad::name_type<short>(),
	                                 ushort_type_name = bad::name_type<unsigned short>(),
	                                 int_type_name = bad::name_type<int>(),
	                                 uint_type_name = bad::name_type<unsigned>(),
	                                 ll_type_name = bad::name_type<long long>(),
	                                 ull_type_name = bad::name_type<unsigned long long>(),
	                                 float_type_name = bad::name_type<float>(),
	                                 double_type_name = bad::name_type<double>(),
	                                 char_array_type_name = bad::name_type<char []>(),
	                                 simple_class_type_name = bad::name_type<simple_class>(),
	                                 nested_class_type_name = bad::name_type<simple_class::nested_class>(),
	                                 namespaced_class_type_name = bad::name_type<ns::namespaced_class>(),
	                                 template_class_type_name = bad::name_type<template_class<simple_class>>(),
	                                 template_template_class_type_name = bad::name_type<template_class<template_class<simple_class>>>(),
	                                 alias_type_name = bad::name_type<alias>();

	std::cout << char_type_name << '\n'
	          << schar_type_name << '\n'
	          << uchar_type_name << '\n'
	          << short_type_name << '\n'
	          << ushort_type_name << '\n'
	          << int_type_name << '\n'
	          << uint_type_name << '\n'
	          << ll_type_name << '\n'
	          << ull_type_name << '\n'
	          << float_type_name << '\n'
	          << double_type_name << '\n'
	          << char_array_type_name << '\n'
	          << simple_class_type_name << '\n'
	          << nested_class_type_name << '\n'
	          << namespaced_class_type_name << '\n'
	          << template_class_type_name << '\n'
	          << template_template_class_type_name << '\n'
	          << alias_type_name << '\n';

	const bool result = char_type_name == "char" &&
	                    schar_type_name == "signed char" &&
	                    uchar_type_name == "unsigned char" &&
	                    float_type_name == "float" &&
	                    double_type_name == "double" &&
#if defined(__GNUC__)
	                    short_type_name == "short int" &&
	                    ushort_type_name == "short unsigned int" &&
	                    int_type_name == "int" &&
	                    uint_type_name == "unsigned int" &&
	                    ll_type_name == "long long int" &&
	                    ull_type_name == "long long unsigned int" &&
	                    char_array_type_name == "char []" &&
	                    simple_class_type_name == "simple_class" &&
	                    nested_class_type_name == "simple_class::nested_class" &&
	                    namespaced_class_type_name == "ns::namespaced_class" &&
	                    template_class_type_name == "template_class<simple_class>" &&
	                    template_template_class_type_name == "template_class<template_class<simple_class> >" &&
	                    alias_type_name == "simple_class"
#elif defined(_MSC_VER)
	                    short_type_name == "short" &&
	                    ushort_type_name == "unsigned short" &&
	                    int_type_name == "int" &&
	                    uint_type_name == "unsigned int" &&
	                    ll_type_name == "__int64" &&
	                    ull_type_name == "unsigned __int64" &&
	                    char_array_type_name == "char[]" &&
	                    simple_class_type_name == "class simple_class" &&
	                    nested_class_type_name == "class simple_class::nested_class" &&
	                    namespaced_class_type_name == "class ns::namespaced_class" &&
	                    template_class_type_name == "class template_class<class simple_class>" &&
	                    template_template_class_type_name == "class template_class<class template_class<class simple_class> >" &&
	                    alias_type_name == "class simple_class"
#endif
	;

	return !result;
}
