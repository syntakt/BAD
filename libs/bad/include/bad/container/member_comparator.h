////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_CONTAINER_MEMBER_COMPARATOR_H
#define BAD_CONTAINER_MEMBER_COMPARATOR_H

#include <bad/type/type_ops.h>
#include <functional>

namespace bad
{
namespace _detail
{

template <member_pointing t>
consteval auto _get_compare_type() noexcept;

}// namespace detail

#define BAD_COMPARE_TYPE decltype(_detail::_get_compare_type<decltype(MP)>())
#define BAD_OPERAND_TYPE typename member_pointer_traits<decltype(MP)>::class_type

template <
#ifdef __GNUC__
	member_pointing // MSVC refuses to compile with the concept
#endif
	auto MP,
	std::predicate<const BAD_COMPARE_TYPE&, const BAD_COMPARE_TYPE&> cmp = std::less<BAD_COMPARE_TYPE>
>
struct member_comparator final {
	bool operator ()(const BAD_OPERAND_TYPE& lhs, const BAD_OPERAND_TYPE& rhs) const noexcept;

	private:
	cmp comparator;
};

}// namespace bad

#include "_detail/member_comparator.inl"

#undef BAD_OPERAND_TYPE
#undef BAD_COMPARE_TYPE

#endif
