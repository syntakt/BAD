////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

namespace bad
{

template <type_variadic<std::tuple> t>
template <unsigned I> requires (I < std::tuple_size<t>())
void tuple_iterator<t>::operator ()(
#ifdef __GNUC__
	std::predicate<std::tuple_element_t<I, t>&>
#endif
	auto&& functor
) noexcept
{
	if (!functor(std::get<I>(tuple))) {
		static constinit const unsigned NEXT = I + 1;
		if constexpr (NEXT < std::tuple_size<t>())
			operator ()<NEXT>(std::forward<decltype(functor)>(functor));
	}
}

template <type_variadic<std::tuple> t>
template <unsigned I> requires (I >= std::tuple_size<t>())
inline void tuple_iterator<t>::operator ()([[maybe_unused]] auto&& functor) noexcept
{
}

}// namespace bad
