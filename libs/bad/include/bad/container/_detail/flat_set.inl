////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <utility>
#include <algorithm>

namespace bad
{

template <typename t, std::predicate<const t&, const t&> c>
flat_set<t, c>::flat_set() noexcept :
	values{}
{
}

template <typename t, std::predicate<const t&, const t&> c>
flat_set<t, c>::flat_set(const flat_set& src) noexcept :
	values{src.values}
{
}

template <typename t, std::predicate<const t&, const t&> c>
flat_set<t, c>::flat_set(flat_set&& src) noexcept :
	values{std::move(src.values)}
{
}

template <typename t, std::predicate<const t&, const t&> c>
flat_set<t, c>::flat_set(const std::initializer_list<value_type>& values) noexcept
{
	for (const value_type& v : values)
		insert(v);
}

template <typename t, std::predicate<const t&, const t&> c>
flat_set<t, c>::~flat_set()
{
}

template <typename t, std::predicate<const t&, const t&> c>
flat_set<t, c>& flat_set<t, c>::operator =(flat_set rhs) noexcept
{
	values = std::move(rhs.values);

	return *this;
}

template <typename t, std::predicate<const t&, const t&> c>
inline const flat_set<t, c>::value_type& flat_set<t, c>::operator [](const std::size_t i) const noexcept
{
	return values[i];
}

template <typename t, std::predicate<const t&, const t&> c>
inline flat_set<t, c>::iterator flat_set<t, c>::begin() const noexcept
{
	return values.begin();
}

template <typename t, std::predicate<const t&, const t&> c>
inline flat_set<t, c>::iterator flat_set<t, c>::end() const noexcept
{
	return values.end();
}

template <typename t, std::predicate<const t&, const t&> c>
inline flat_set<t, c>::reverse_iterator flat_set<t, c>::rbegin() const noexcept
{
	return values.rbegin();
}

template <typename t, std::predicate<const t&, const t&> c>
inline flat_set<t, c>::reverse_iterator flat_set<t, c>::rend() const noexcept
{
	return values.rend();
}

template <typename t, std::predicate<const t&, const t&> c>
inline std::size_t flat_set<t, c>::size() const noexcept
{
	return values.size();
}

template <typename t, std::predicate<const t&, const t&> c>
inline bool flat_set<t, c>::empty() const noexcept
{
	return values.empty();
}

template <typename t, std::predicate<const t&, const t&> c>
flat_set<t, c>::iterator flat_set<t, c>::find(const value_type& value) const noexcept
{
	const c compare{};
	const iterator it = std::lower_bound(begin(), end(), value, compare);
	return it == end() || compare(value, *it) ? end() : it;
}

template <typename t, std::predicate<const t&, const t&> c>
std::pair<typename flat_set<t, c>::iterator, bool> flat_set<t, c>::insert(value_type value) noexcept
{
	bool inserted = false;
	iterator it;

	const c compare{};
	if (it = std::lower_bound(begin(), end(), value, compare);
	    it == end() || compare(value, *it)) {
		it = values.insert(it, std::move(value));
		inserted = true;
	}

	return {it, inserted};
}

template <typename t, std::predicate<const t&, const t&> c>
inline flat_set<t, c>::iterator flat_set<t, c>::erase(const iterator& it) noexcept
{
	return values.erase(it);
}

template <typename t, std::predicate<const t&, const t&> c>
std::size_t flat_set<t, c>::erase(const value_type& value) noexcept
{
	iterator it = find(value);
	std::size_t n = 0;
	if (it != end()) {
		erase(it);
		n = 1;
	}
	return n;
}

template <typename t, std::predicate<const t&, const t&> c>
inline const flat_set<t, c>::value_type* flat_set<t, c>::data() const noexcept
{
	return values.data();
}

template <typename t, std::predicate<const t&, const t&> c>
inline void flat_set<t, c>::clear() noexcept
{
	return values.clear();
}

}// namespace bad
