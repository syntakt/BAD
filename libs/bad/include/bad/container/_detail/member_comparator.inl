////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

namespace bad
{
namespace _detail
{

template <member_pointing t>
inline consteval auto _get_compare_type() noexcept
{
	using t_member_pointer_traits = member_pointer_traits<t>;
	if constexpr (member_object_pointing<t>)
		return typename t_member_pointer_traits::value_type{};
	else
		return typename t_member_pointer_traits::return_type{};
}

}// namespace detail

template <
#ifdef __GNUC__
	member_pointing
#endif
	auto MP,
	std::predicate<const BAD_COMPARE_TYPE&, const BAD_COMPARE_TYPE&> cmp
>
inline bool member_comparator<MP, cmp>::operator ()(const BAD_OPERAND_TYPE& lhs, const BAD_OPERAND_TYPE& rhs) const noexcept
{
	if constexpr (member_object_pointing<decltype(MP)>)
		return comparator(lhs.*MP, rhs.*MP);
	else
		return comparator((lhs.*MP)(), (rhs.*MP)());
}

}// namespace bad
