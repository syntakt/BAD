////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_CONTAINER_TUPLE_ITERATOR_H
#define BAD_CONTAINER_TUPLE_ITERATOR_H

#include <bad/type/concepts.h>
#include <tuple>

namespace bad
{

template <type_variadic<std::tuple> t>
struct tuple_iterator final {
	template <unsigned I = 0> requires (I < std::tuple_size<t>())
	void operator ()(
#ifdef __GNUC__
		std::predicate<std::tuple_element_t<I, t>&> // MSVC fails concept short-circuiting
#endif
		auto&& functor
	) noexcept;

	template <unsigned I = 0> requires (I >= std::tuple_size<t>())
	void operator ()(auto&& functor) noexcept;

	t& tuple;
};

}// namespace bad

#include "_detail/tuple_iterator.inl"

#endif
