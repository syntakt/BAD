////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_CONTAINER_FLAT_SET_H
#define BAD_CONTAINER_FLAT_SET_H

#include <concepts>
#include <functional>
#include <vector>

namespace bad
{

template <typename t, std::predicate<const t&, const t&> c = std::less<t>>
class flat_set {
	public:
	using value_type = t;

	using iterator = std::vector<value_type>::const_iterator;

	using reverse_iterator = std::vector<value_type>::const_reverse_iterator;

	using key_compare = c;

	flat_set() noexcept;

	flat_set(const flat_set& src) noexcept;

	flat_set(flat_set&& src) noexcept;

	flat_set(const std::initializer_list<value_type>& values) noexcept;

	~flat_set();

	flat_set& operator =(flat_set rhs) noexcept;

	const value_type& operator [](const std::size_t i) const noexcept;

	iterator begin() const noexcept;

	iterator end() const noexcept;

	reverse_iterator rbegin() const noexcept;

	reverse_iterator rend() const noexcept;

	std::size_t size() const noexcept;

	bool empty() const noexcept;

	iterator find(const value_type& value) const noexcept;

	std::pair<iterator, bool> insert(value_type value) noexcept;

	iterator erase(const iterator& it) noexcept;

	std::size_t erase(const value_type& value) noexcept;

	const value_type* data() const noexcept;

	void clear() noexcept;

	private:
	std::vector<value_type> values;
};

}// namespace bad

#include "_detail/flat_set.inl"

#endif
