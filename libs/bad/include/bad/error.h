////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_ERROR_H
#define BAD_ERROR_H

#include <source_location>
#include <exception>
#include <string>

#define BAD_ERROR(message)\
::bad::error{std::string{std::source_location::current().file_name()} + ": " +\
             std::to_string(std::source_location::current().line()) + ": " +\
             std::source_location::current().function_name() + ": " +\
             message}

namespace bad
{

class error : public std::exception {
	public:
	error(const std::string& message = "") noexcept;

	error(const error& src) noexcept;

	error(error&& src) noexcept;

	~error() override;

	error& operator =(error rhs) noexcept;

	const char* what() const noexcept override;

	private:
	std::string message;
};

}// namespace bad

#endif
