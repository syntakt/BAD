////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_STRING_INDENT_H
#define BAD_STRING_INDENT_H

#include <string>

namespace bad
{

class indent final {
	public:
	indent(const std::size_t n = 0) noexcept;

	indent(const indent& src) noexcept;

	indent(indent&& src) noexcept;

	~indent();

	indent& operator =(indent rhs) noexcept;

	indent& operator ++() noexcept;

	indent& operator --() noexcept;

	const std::string& get() const noexcept;

	private:
	std::string string;
};

}// namespace bad

#include "_detail/indent.inl"

#endif
