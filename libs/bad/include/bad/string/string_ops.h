////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_STRING_STRING_OPS_H
#define BAD_STRING_STRING_OPS_H

#include <string>
#include <string_view>

namespace bad::str
{

std::size_t replace_all(std::string& str, const std::string_view& what, const std::string_view& with) noexcept;

std::string convert_view(const std::string_view& string_view) noexcept;

}// namespace bad::str

#endif
