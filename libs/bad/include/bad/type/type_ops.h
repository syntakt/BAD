////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_TYPE_TYPE_OPS_H
#define BAD_TYPE_TYPE_OPS_H

#include "concepts.h"
#include "type_list.h"

namespace bad
{

template <typename t>
struct remove_pointers final {
	using type = t;
};

template <lvalue_referencing t>
struct remove_pointers<t> final {
	using type = std::add_lvalue_reference_t<typename remove_pointers<std::remove_reference_t<t>>::type>;
};

template <rvalue_referencing t>
struct remove_pointers<t> final {
	using type = std::add_rvalue_reference_t<typename remove_pointers<std::remove_reference_t<t>>::type>;
};

template <pointing t>
struct remove_pointers<t> final {
	using type = remove_pointers<std::remove_pointer_t<t>>::type;
};

template <typename t>
struct nude final {
	using type = std::remove_cvref_t<typename remove_pointers<t>::type>;
};

template <member_pointing>
struct member_pointer_traits;

template <typename t, structural c> requires member_object_pointing<t c::*>
struct member_pointer_traits<t c::*> final {
	using value_type = t;

	using class_type = c;
};

#define BAD_MEMBER_FUNCTION_POINTER_TRAITS(qualifiers)\
template <typename r, structural c, typename... ts> requires member_function_pointing<r (c::*)(ts...) qualifiers>\
struct member_pointer_traits<r (c::*)(ts...) qualifiers> final {\
	using return_type = r;\
\
	using class_type = c;\
\
	using param_types = type_list<ts...>;\
}

BAD_MEMBER_FUNCTION_POINTER_TRAITS();

BAD_MEMBER_FUNCTION_POINTER_TRAITS(noexcept);

BAD_MEMBER_FUNCTION_POINTER_TRAITS(const);

BAD_MEMBER_FUNCTION_POINTER_TRAITS(const noexcept);

#undef BAD_MEMBER_FUNCTION_POINTER_TRAITS

}// namespace bad

#endif
