////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_TYPE_TYPE_LIST_H
#define BAD_TYPE_TYPE_LIST_H

#include <concepts>

namespace bad
{

template <typename... ts>
struct type_list;

template <>
struct type_list<> final {
	template <typename... qs> requires (sizeof...(qs) == 0)
	static consteval bool contains() noexcept;
};

template <typename t, typename... ts>
struct type_list<t, ts...> final {
	using head = t;

	using tail = type_list<ts...>;

	struct element final {
		using type = head;
	};

	template <std::predicate<const typename type_list<t, ts...>::element&> f>
	static constexpr void for_each(f&& functor) noexcept;

	template <typename q, typename... qs> requires (sizeof...(qs) == sizeof...(ts))
	static consteval bool contains() noexcept;
};

template <typename... ts, typename... us>
consteval type_list<ts..., us...> operator +(const type_list<ts...>& lhs, const type_list<us...>& rhs) noexcept;

}// namespace bad

#include "_detail/type_list.inl"

#endif
