////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_TYPE_TYPE_LIST_EXPANDER_H
#define BAD_TYPE_TYPE_LIST_EXPANDER_H

#include <vector>
#include <any>
#include "concepts.h"
#include "type_list.h"

namespace bad
{

template <type_variadic<type_list> tl>
struct type_list_expander;

template <typename... ts>
struct type_list_expander<type_list<ts...>> final {
	template <std::invocable<ts...> f>
	constexpr void operator ()(f&& functor, const std::vector<std::any>& args) const noexcept;

	private:
	template <std::invocable<ts...> f, std::size_t... Is>
	constexpr void expand(f&& functor, const std::vector<std::any>& args, const std::index_sequence<Is...>& sequence) const noexcept;
};

}// namespace bad

#include "_detail/type_list_expander.inl"

#endif
