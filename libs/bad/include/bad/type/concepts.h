////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_TYPE_CONCEPTS_H
#define BAD_TYPE_CONCEPTS_H

#include <concepts>

namespace bad
{

template <typename t>
concept arithmetic = std::is_arithmetic_v<t>;

template <typename t>
concept referencing = std::is_reference_v<t>;

template <typename t>
concept lvalue_referencing = std::is_lvalue_reference_v<t>;

template <typename t>
concept rvalue_referencing = std::is_rvalue_reference_v<t>;

template <typename t>
concept pointing = std::is_pointer_v<t>;

template <typename t>
concept member_pointing = std::is_member_pointer_v<t>;

template <typename t>
concept member_object_pointing = std::is_member_object_pointer_v<t>;

template <typename t>
concept member_function_pointing = std::is_member_function_pointer_v<t>;

template <typename t>
concept structural = std::is_class_v<t>;

namespace _detail
{

template <typename t, template <typename...> typename u>
struct _type_variadic_detector final : std::false_type {};

template <template <typename...> typename u, typename... ts>
struct _type_variadic_detector<u<ts...>, u> final : std::true_type {};

}// namespace _detail

template <typename t, template <typename...> typename u>
concept type_variadic = _detail::_type_variadic_detector<std::remove_const_t<t>, u>::value;

template <typename f, typename r, typename... ts>
concept invocable_r = std::is_invocable_r_v<r, f, ts...>;

template <typename t>
concept smart_pointing = requires (t x) {
	typename t::element_type;

	{*x} -> std::convertible_to<typename t::element_type&>;

	{x.t::operator ->()} -> std::convertible_to<typename t::element_type*>;

	{x.get()} -> std::convertible_to<typename t::element_type*>;

	{static_cast<bool>(x)} -> std::convertible_to<bool>;
};

}// namespace bad

#endif
