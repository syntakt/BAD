////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_TYPE_TYPE_H
#define BAD_TYPE_TYPE_H

#include <string_view>

namespace bad
{

using type_id = const void*;

struct type final {
	type_id id;

	std::string_view name;
};

template <typename t>
consteval type_id id_type() noexcept;

template <typename t>
consteval std::string_view name_type() noexcept;

template <typename t>
consteval type get_type() noexcept;

}// namespace bad

#include "_detail/type.inl"

#endif
