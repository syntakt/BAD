////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifdef __GNUC__
#include <source_location>
#endif

namespace bad
{
namespace _detail
{

inline consteval std::string_view _extract_type_name(const char* ptr) noexcept
{
#if defined(__GNUC__)
	while (*ptr++ != '=');
	while (*ptr == ' ')
		++ptr;
	const char* const start = ptr;
	while (*ptr != ';')
		++ptr;
#elif defined(_MSC_VER)
	while (*ptr++ != '>');
	while (*ptr++ != '>');
	while (*ptr++ != '<');
	const char* const start = ptr;
	while (*ptr != '(')
		++ptr;
	--ptr;
#endif

	return {start, static_cast<std::size_t>(ptr - start)};
}

}// namespace _detail

template <typename t>
inline consteval type_id id_type() noexcept
{
	return name_type<t>().data();
}

template <typename t>
inline consteval std::string_view name_type() noexcept
{
	return _detail::_extract_type_name(
#if defined(__GNUC__)
		std::source_location::current().function_name()
#elif defined(_MSC_VER)
		__FUNCSIG__
#endif
	);
}

template <typename t>
inline consteval type get_type() noexcept
{
	return {id_type<t>(), name_type<t>()};
}

}// namespace bad
