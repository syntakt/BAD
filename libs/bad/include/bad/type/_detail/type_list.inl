////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <utility>

namespace bad
{

template <typename... qs> requires (sizeof...(qs) == 0)
consteval bool type_list<>::contains() noexcept
{
	return true;
}

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4390)
#endif
template <typename t, typename... ts>
template <std::predicate<const typename type_list<t, ts...>::element&> f>
constexpr void type_list<t, ts...>::for_each(f&& functor) noexcept
{ 
	if (!functor(element{}))
		if constexpr (requires { typename tail::head; })
			tail::for_each(std::forward<f>(functor));
}
#ifdef _MSC_VER
#pragma warning(pop)
#endif

template <typename t, typename... ts>
template <typename q, typename... qs> requires (sizeof...(qs) == sizeof...(ts))
consteval bool type_list<t, ts...>::contains() noexcept
{
	bool tail_result = true;
	if constexpr (requires { typename tail::head; } && sizeof...(qs))
		tail_result = tail::template contains<qs...>();
	return std::is_convertible_v<q, t> && tail_result;
}

template <typename... ts, typename... us>
consteval type_list<ts..., us...> operator +([[maybe_unused]] const type_list<ts...>& lhs, [[maybe_unused]] const type_list<us...>& rhs) noexcept
{
	return type_list<ts..., us...>{};
}

}// namespace bad
