////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

namespace bad
{

template <typename... ts>
template <std::invocable<ts...> f>
inline constexpr void type_list_expander<type_list<ts...>>::operator ()(f&& functor, const std::vector<std::any>& args) const noexcept
{
	expand(functor, args, std::make_index_sequence<sizeof...(ts)>{});
}

template <typename... ts>
template <std::invocable<ts...> f, std::size_t... Is>
inline constexpr void type_list_expander<type_list<ts...>>::expand(f&& functor, const std::vector<std::any>& args, [[maybe_unused]] const std::index_sequence<Is...>& sequence) const noexcept
{
	functor(std::any_cast<ts>(args[Is])...);
}

}// namespace bad
