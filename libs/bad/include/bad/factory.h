////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef BAD_FACTORY_H
#define BAD_FACTORY_H

#include <functional>
#include <string>
#include <map>

namespace bad
{

template <typename t>
class factory {
	public:
	using creation_type = t;

	using creator_type = std::function<creation_type ()>;

	factory() noexcept;

	factory(const factory& src) noexcept;

	factory(factory&& src) noexcept;

	~factory();

	factory& operator =(factory rhs) noexcept;

	void register_creator(const std::string& creator_name, const creator_type& creator) noexcept;

	void deregister_creator(const std::string& creator_name) noexcept;

	creation_type create(const std::string& creator_name) const noexcept;

	private:
	std::map<std::string, creator_type> creators;
};

}// namespace bad

#include "_detail/factory.inl"

#endif
