////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <utility>

namespace bad
{

template <typename t>
factory<t>::factory() noexcept :
	creators{}
{
}

template <typename t>
factory<t>::factory(const factory& src) noexcept :
	creators{src.creators}
{
}

template <typename t>
factory<t>::factory(factory&& src) noexcept :
	creators{std::move(src.creators)}
{
}

template <typename t>
factory<t>::~factory()
{
}

template <typename t>
factory<t>& factory<t>::operator =(factory rhs) noexcept
{
	creators = std::move(rhs.creators);

	return *this;
}

template <typename t>
inline void factory<t>::register_creator(const std::string& creator_name, const creator_type& creator) noexcept
{
	creators[creator_name] = creator;
}

template <typename t>
inline void factory<t>::deregister_creator(const std::string& creator_name) noexcept
{
	creators.erase(creator_name);
}

template <typename t>
factory<t>::creation_type factory<t>::create(const std::string& creator_name) const noexcept
{
	creation_type object = nullptr;

	const typename std::map<std::string, creator_type>::const_iterator it = creators.find(creator_name);
	if (it != creators.end())
		object = it->second();

	return object;
}

}// namespace bad
