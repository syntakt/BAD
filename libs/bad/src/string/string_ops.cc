////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/string/string_ops.h>

namespace bad::str
{

std::size_t replace_all(std::string& str, const std::string_view& what, const std::string_view& with) noexcept
{
	std::size_t count = 0;

	for (std::string::size_type pos{}; (pos = str.find(what.data(), pos, what.length())) != std::string::npos; pos += with.length(), ++count)
		str.replace(pos, what.length(), with.data(), with.length());

	return count;
}

std::string convert_view(const std::string_view& string_view) noexcept
{
	return std::string{string_view.data(), string_view.size()};
}

}// namespace bad::str
