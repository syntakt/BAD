////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/string/indent.h>
#include <utility>

namespace bad
{

indent::indent(const std::size_t n) noexcept :
	string{}
{
	string.append(n, '\t');
}

indent::indent(const indent& src) noexcept :
	string{src.string}
{
}

indent::indent(indent&& src) noexcept :
	string{std::move(src.string)}
{
}

indent::~indent()
{
}

indent& indent::operator =(indent rhs) noexcept
{
	string = std::move(rhs.string);

	return *this;
}

indent& indent::operator ++() noexcept
{
	string.push_back('\t');
	return *this;
}

indent& indent::operator --() noexcept
{
	if (!string.empty())
		string.pop_back();
	return *this;
}

}// namespace bad
