////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <bad/error.h>
#include <utility>

namespace bad
{

error::error(const std::string& message) noexcept :
	exception{},
	message{message}
{
}

error::error(const error& src) noexcept :
	exception{src},
	message{src.message}
{
}

error::error(error&& src) noexcept :
	exception{std::move(src)},
	message{std::move(src.message)}
{
}

error::~error()
{
}

error& error::operator =(error rhs) noexcept
{
	exception::operator =(std::move(rhs));

	message = std::move(rhs.message);

	return *this;
}

const char* error::what() const noexcept
{
	return message.c_str();
}

}// namespace bad
