////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef IO_TEXT_LOADER_H
#define IO_TEXT_LOADER_H

#include "loader.h"

namespace io
{

class text_loader : public loader {
	public:
	text_loader(const std::string& path);

	text_loader(const text_loader& src) noexcept = delete;

	text_loader(text_loader&& src) noexcept;

	~text_loader() override;

	text_loader& operator =(const text_loader& rhs) noexcept = delete;

	text_loader& operator =(text_loader&& rhs) noexcept;

	std::string operator ()() noexcept;
};

}// namespace io

#endif
