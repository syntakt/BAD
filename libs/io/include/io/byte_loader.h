////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef IO_BYTE_LOADER_H
#define IO_BYTE_LOADER_H

#include "loader.h"

namespace io
{

class byte_loader : public loader {
	public:
	byte_loader(const std::string& path);

	byte_loader(const byte_loader& src) noexcept = delete;

	byte_loader(byte_loader&& src) noexcept;

	~byte_loader() override;

	byte_loader& operator =(const byte_loader& rhs) noexcept = delete;

	byte_loader& operator =(byte_loader&& rhs) noexcept;

	std::string operator ()() noexcept;
};

}// namespace io

#endif
