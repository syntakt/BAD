////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef IO_LOADER_H
#define IO_LOADER_H

#include <fstream>

namespace io
{

class loader {
	public:
	loader(const std::string& path, const std::ios::openmode mode = std::ios_base::in);

	loader(const loader& src) noexcept = delete;

	loader(loader&& src) noexcept;

	virtual ~loader();

	loader& operator =(const loader& rhs) noexcept = delete;

	loader& operator =(loader&& rhs) noexcept;

	protected:
	std::ifstream input;
};

}// namespace io

#endif
