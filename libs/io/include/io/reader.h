////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef IO_READER_H
#define IO_READER_H

namespace io
{

class reader {
	public:
	reader(const char* const input = nullptr) noexcept;

	reader(const reader& src) noexcept;

	reader(reader&& src) noexcept;

	virtual ~reader();

	reader& operator =(reader rhs) noexcept;

	const char* get_input() const noexcept;

	void set_input(const char* const input) noexcept;

	private:
	const char* input;
};

}// namespace io

#include "_detail/reader.inl"

#endif
