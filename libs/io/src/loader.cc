////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <io/loader.h>
#include <bad/error.h>
#include <utility>

namespace io
{

loader::loader(const std::string& path, const std::ios::openmode mode) :
	input{path, mode}
{
	if (!input.is_open())
		throw BAD_ERROR("std::ifstream::is_open: input error on \"" + path + '\"');
}

loader::loader(loader&& src) noexcept :
	input{std::move(src.input)}
{
}

loader::~loader()
{
	input.close();
}

loader& loader::operator =(loader&& rhs) noexcept
{
	input = std::move(rhs.input);

	return *this;
}

}// namespace io
