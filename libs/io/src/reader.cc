////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <io/reader.h>

namespace io
{

reader::reader(const char* const input) noexcept :
	input{input}
{
}

reader::reader(const reader& src) noexcept :
	input{src.input}
{
}

reader::reader(reader&& src) noexcept :
	input{src.input}
{
	src.input = nullptr;
}

reader::~reader()
{
}

reader& reader::operator =(reader rhs) noexcept
{
	input = rhs.input;

	rhs.input = nullptr;

	return *this;
}

}// namespace io
