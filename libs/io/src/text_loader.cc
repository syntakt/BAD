////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <io/text_loader.h>
#include <utility>

namespace io
{

text_loader::text_loader(const std::string& path) :
	loader{path}
{
}

text_loader::text_loader(text_loader&& src) noexcept :
	loader{std::move(src)}
{
}

text_loader::~text_loader()
{
}

text_loader& text_loader::operator =(text_loader&& rhs) noexcept
{
	loader::operator =(std::move(rhs));

	return *this;
}

std::string text_loader::operator ()() noexcept
{
	return {{std::istreambuf_iterator<char>{input}}, std::istreambuf_iterator<char>{}};
}

}// namespace io
