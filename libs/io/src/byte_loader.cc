////////////////////////////////////////////////////////////////////////////////////////
//
// Copyright 2022 syntakt
// All rights reserved
//
////////////////////////////////////////////////////////////////////////////////////////

#include <io/byte_loader.h>
#include <utility>

namespace io
{

byte_loader::byte_loader(const std::string& path) :
	loader{path, std::ios::ate | std::ios::binary}
{
}

byte_loader::byte_loader(byte_loader&& src) noexcept :
	loader{std::move(src)}
{
}

byte_loader::~byte_loader()
{
}

byte_loader& byte_loader::operator =(byte_loader&& rhs) noexcept
{
	loader::operator =(std::move(rhs));

	return *this;
}

std::string byte_loader::operator ()() noexcept
{
	const std::size_t bc = static_cast<std::size_t>(input.tellg());
	std::string buffer(bc, '\0');
	input.seekg(0);
	input.read(buffer.data(), static_cast<std::streamsize>(bc));
	return buffer;
}

}// namespace io
